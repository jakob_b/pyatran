# this is based on https://github.com/ogrisel/python-appveyor-demo

matrix:
  allow_failures:
    # This fails with an "ImportError: DLL load failed" at
    # "from . import multiarray"
    - CI_PY_ID: "py34-32"
    # The following fail because pycodestyle=2.0 isn't available for py36
    - CI_PY_ID: "py36-32"
    - CI_PY_ID: "py36-64"

environment:
  global:
    # SDK v7.0 MSVC Express 2008's SetEnv.cmd script will fail if the
    # /E:ON and /V:ON options are not enabled in the batch script intepreter
    # See: http://stackoverflow.com/a/13751649/163740
    CMD_IN_ENV: "cmd /E:ON /V:ON /C .\\appveyor\\run_with_env.cmd"

  matrix:

    # Pre-installed Python versions, which Appveyor may upgrade to
    # a later point release.
    # See: http://www.appveyor.com/docs/installed-software#python

    - CI_PY_ID: "py27-32"
      PYTHON: "C:\\Miniconda"
      PYTHON_VERSION: "2.7.x"
      PYTHON_ARCH: "32"

    - CI_PY_ID: "py27-64"
      PYTHON: "C:\\Miniconda-x64"
      PYTHON_VERSION: "2.7.x"
      PYTHON_ARCH: "64"

    - CI_PY_ID: "py34-32"
      PYTHON: "C:\\Miniconda3"
      PYTHON_VERSION: "3.4.x"
      PYTHON_ARCH: "32"

    - CI_PY_ID: "py34-64"
      PYTHON: "C:\\Miniconda3-x64"
      PYTHON_VERSION: "3.4.x"
      PYTHON_ARCH: "64"

    - CI_PY_ID: "py35-32"
      PYTHON: "C:\\Miniconda35"
      PYTHON_VERSION: "3.5.x"
      PYTHON_ARCH: "32"

    - CI_PY_ID: "py35-64"
      PYTHON: "C:\\Miniconda35-x64"
      PYTHON_VERSION: "3.5.x"
      PYTHON_ARCH: "64"

    - CI_PY_ID: "py36-32"
      PYTHON: "C:\\Miniconda36"
      PYTHON_VERSION: "3.6.x"
      PYTHON_ARCH: "32"

    - CI_PY_ID: "py36-64"
      PYTHON: "C:\\Miniconda36-x64"
      PYTHON_VERSION: "3.6.x"
      PYTHON_ARCH: "64"

install:
  # If there is a newer build queued for the same PR, cancel this one.
  # The AppVeyor 'rollout builds' option is supposed to serve the same
  # purpose but it is problematic because it tends to cancel builds pushed
  # directly to master instead of just PR builds (or the converse).
  # credits: JuliaLang developers.
  - ps: if ($env:APPVEYOR_PULL_REQUEST_NUMBER -and $env:APPVEYOR_BUILD_NUMBER -ne ((Invoke-RestMethod `
        https://ci.appveyor.com/api/projects/$env:APPVEYOR_ACCOUNT_NAME/$env:APPVEYOR_PROJECT_SLUG/history?recordsNumber=50).builds | `
        Where-Object pullRequestId -eq $env:APPVEYOR_PULL_REQUEST_NUMBER)[0].buildNumber) { `
          throw "There are newer queued builds for this pull request, failing early." }
  - ECHO "Filesystem root:"
  - ps: "ls \"C:/\""

  - ECHO "Installed SDKs:"
  - ps: "ls \"C:/Program Files/Microsoft SDKs/Windows\""

  # Prepend newly installed Python to the PATH of this build (this cannot be
  # done from inside the powershell script as it would require to restart
  # the parent CMD process).
  - "SET PATH=%PYTHON%;%PYTHON%\\Scripts;%PATH%"

  # Check that we have the expected version and architecture for Python
  - "python --version"
  - "python -c \"import struct; print(struct.calcsize('P') * 8)\""

  # Configure conda
  - "%CMD_IN_ENV% conda config --set always_yes yes --set changeps1 no"
  # Upgrade to the latest version of conda.  We need this for "--append"
  - "%CMD_IN_ENV% conda update -q conda"
  # Add conda-forge channel
  - "%CMD_IN_ENV% conda config --append channels conda-forge"
  - "%CMD_IN_ENV% conda install -q conda-build"

  # Display conda information
  - "%CMD_IN_ENV% conda info -a"

  # Install the dependencies.
  - "%CMD_IN_ENV% conda install -q --file requirements_appveyor.txt"

build: off

test_script:
  # Run the project tests
  - "%CMD_IN_ENV% python setup.py test"
