.. Project Status
.. image:: https://img.shields.io/pypi/status/pyatran.svg
   :target: https://pypi.python.org/pypi/pyatran/
.. PyPI package
.. image:: https://img.shields.io/pypi/v/pyatran.svg
   :target: https://pypi.python.org/pypi/pyatran/
.. Anaconda package
.. image:: https://img.shields.io/conda/v/andreas-h/pyatran.svg
   :target: https://anaconda.org/andreas-h/pyatran
.. Python Versions
.. image:: https://img.shields.io/pypi/pyversions/pyatran.svg
   :target: https://pypi.python.org/pypi/pyatran/
.. License - license badge must use gh repo, as shields.io doesn't support gl
.. image:: https://img.shields.io/github/license/andreas-h/emiprep.svg
   :target: https://gitlab.com/andreas-h/pyatran/blob/develop/LICENSE
   :alt: License: AGPLv3
.. Build Status (develop branch)
.. image:: https://gitlab.com/andreas-h/pyatran/badges/develop/pipeline.svg
   :target: https://gitlab.com/andreas-h/pyatran/commits/develop
   :alt: Build Status
.. image:: https://ci.appveyor.com/api/projects/status/yscofaiofit0tux6?svg=true
   :target: https://ci.appveyor.com/project/andreas-h/pyatran
   :alt: Build Status (Windows)
.. CodeCov            
.. image:: https://codecov.io/gl/andreas-h/pyatran/branch/develop/graph/badge.svg
   :target: https://codecov.io/gl/andreas-h/pyatran
   :alt: Coverage Report
.. Codacy
.. image:: https://api.codacy.com/project/badge/Grade/4bb47f51da6448a09fe09fa2edca93b9
   :target: https://www.codacy.com/app/andreas-h/pyatran?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=andreas-h/pyatran&amp;utm_campaign=Badge_Grade
   :alt: Codacy Report


========================================
pyatran -- handling SCIATRAN from Python
========================================

pyatran is a collection of Python methods and classes to access the `SCIATRAN
radiative transfer model <http://www.iup.uni-bremen.de/sciatran/>`__ from the
`Python <https://www.python.org>`__ programming language.


Reporting bugs
--------------

For reporting bugs, please use the `issue tracker
<https://gitlab.com/andreas-h/pyatran/issues>`__ on Gitlab.  In case you don't
have an account on Gitlab, you can also `send an e-mail
<mailto:incoming+andreas-h/pyatran@gitlab.com>`__ to create an issue.


Development
-----------

Starting after release 0.1.8, pyatran follows the git branching model as
described in the excellent `blog article by Vincent Driessen
<http://nvie.com/posts/a-successful-git-branching-model/>`__.
